#!/bin/sh

# Utilisateur

if [ ! -d "/home/$APP_USR" ]; then
    /usr/sbin/addgroup --gid $APP_GID $APP_GRP

    /usr/sbin/adduser -D \
        -g "Utilisateur $APP_USR" \
        -G $APP_GRP \
        -u $APP_UID \
        -h /home/$APP_USR \
        $APP_USR

    chown -R $APP_USR:$APP_GRP /var/www/html
fi

# HTTPD

HTTPD_DIR='/usr/local/apache2'

sed -i "s~User daemon~User $APP_USR~g" $HTTPD_DIR/conf/httpd.conf
sed -i "s~Group daemon~Group $APP_GRP~g" $HTTPD_DIR/conf/httpd.conf

sed -i 's~DocumentRoot "/usr/local/apache2/htdocs"~DocumentRoot "/var/www/html"~g' $HTTPD_DIR/conf/httpd.conf

sed -i "s~__APP_FPM__~$APP_FPM~g" $HTTPD_DIR/conf.d/zzz-app.conf

# ... démarrage de httpd ...
httpd-foreground
