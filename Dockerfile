
FROM httpd:2.4-alpine

ENV APP_ENV 'prod'
ENV APP_TMZ 'Europe/Paris'
ENV APP_USR ''
ENV APP_UID ''
ENV APP_GRP ''
ENV APP_GID ''
ENV APP_FPM ''

RUN apk update

# Préparation conf HTTPD

RUN mkdir -p /var/www/html

RUN mkdir /usr/local/apache2/conf.d

COPY ./conf.d/ /usr/local/apache2/conf.d/

RUN echo 'IncludeOptional conf.d/*.conf' >> /usr/local/apache2/conf/httpd.conf

RUN sed -i '/LoadModule expires_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule deflate_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule headers_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule proxy_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule proxy_fcgi_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule rewrite_module/s/^#//g' /usr/local/apache2/conf/httpd.conf \
    && sed -i '/LoadModule remoteip_module/s/^#//g' /usr/local/apache2/conf/httpd.conf

RUN sed -i '/DirectoryIndex index.html/s/index\.html/index\.php index\.html/g' /usr/local/apache2/conf/httpd.conf

# Scripts

COPY ./scripts/ /scripts/
RUN chmod 700 /scripts/*.sh

# Démarrage

WORKDIR /var/www/html
CMD ["/scripts/run.sh"]
